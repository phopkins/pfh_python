## MAKE SURE YOU SET THE ENVIRONMENT VARIABLE 'PYDIR' = directory with all the PFH routines
##
## this loads various parts of my python libraries under one 'parent' module heading
##  -- this is purely for convenience, any of these can be called on their own --
##

import numpy as np
import math
from scipy.sparse import coo_matrix
from scipy.signal import convolve2d, convolve, gaussian
import colors_sps.colors_table as ctab
import colors_sps.lum_mag_conversions as lmag
import attenuation.attenuate_wrapper as atten
import attenuation.cross_section as atten_cx
import agn_spectrum.agn_spectrum_wrapper as agnspec

def return_python_routines_homedir():
    import os
    return os.environ['PYDIR']
    ## make sure this is set to the pfh_python routines directory, 
    ##   as an environment variable, since everything will link through it! otherwise nothing works
    ## alternatively, you can manually set this here, but you must remember to change the code 
    ##   on every machine you install onto !
    ##return '/Users/phopkins/Documents/work/plots/python'

def return_python_routines_cdir():
    return return_python_routines_homedir()+'/c_libraries'

def get_default_args(func):
    import inspect
    signature = inspect.signature(func)
    return {
        k: v.default
        for k, v in signature.parameters.items()
        if v.default is not inspect.Parameter.empty
    }

    
## routines from colors_sps module
def colors_table( age_in_Gyr, metallicity_in_solar_units, 
    BAND_ID=0, SALPETER_IMF=0, CHABRIER_IMF=1, QUIET=1, CRUDE=0, 
    RETURN_NU_EFF=0, RETURN_LAMBDA_EFF=0, UNITS_SOLAR_IN_BAND=0 ):
    return ctab.colors_table( age_in_Gyr, metallicity_in_solar_units, 
        BAND_ID=BAND_ID, SALPETER_IMF=SALPETER_IMF, CHABRIER_IMF=CHABRIER_IMF, QUIET=QUIET, CRUDE=CRUDE, 
        RETURN_NU_EFF=RETURN_NU_EFF, RETURN_LAMBDA_EFF=RETURN_LAMBDA_EFF, UNITS_SOLAR_IN_BAND=UNITS_SOLAR_IN_BAND )

def get_solar_mags():
    return lmag.get_solar_mags();
    
def luminosity_to_magnitude( L, \
	UNITS_SOLAR_BOL=0, UNITS_SOLAR_BAND=0, \
	UNITS_CGS=0, \
	NU_L_NU=0, L_NU=0, \
	BAND_U=0,BAND_B=0,BAND_V=0,BAND_R=0,BAND_I=0, \
	BAND_J=0,BAND_H=0,BAND_K=0,BAND_SDSS_u=0, \
	BAND_SDSS_g=0,BAND_SDSS_r=0,BAND_SDSS_i=0, \
	BAND_SDSS_z=0,BOLOMETRIC=0, BAND_NUMBER=0, \
	VEGA=0, AB=0 ):
    return lmag.luminosity_to_magnitude( L, 
	UNITS_SOLAR_BOL=UNITS_SOLAR_BOL, UNITS_SOLAR_BAND=UNITS_SOLAR_BAND, 
	UNITS_CGS=UNITS_CGS, 
	NU_L_NU=NU_L_NU, L_NU=L_NU, 
	BAND_U=BAND_U,BAND_B=BAND_B,BAND_V=BAND_V,BAND_R=BAND_R,BAND_I=BAND_I,
	BAND_J=BAND_J,BAND_H=BAND_H,BAND_K=BAND_K,BAND_SDSS_u=BAND_SDSS_u, 
	BAND_SDSS_g=BAND_SDSS_g,BAND_SDSS_r=BAND_SDSS_r,BAND_SDSS_i=BAND_SDSS_i, 
	BAND_SDSS_z=BAND_SDSS_z,BOLOMETRIC=BOLOMETRIC, BAND_NUMBER=BAND_NUMBER, 
	VEGA=VEGA, AB=AB, MAGNITUDE_TO_LUMINOSITY=0)
	
def magnitude_to_luminosity( L, \
	UNITS_SOLAR_BOL=0, UNITS_SOLAR_BAND=0, \
	UNITS_CGS=0, \
	NU_L_NU=0, L_NU=0, \
	BAND_U=0,BAND_B=0,BAND_V=0,BAND_R=0,BAND_I=0, \
	BAND_J=0,BAND_H=0,BAND_K=0,BAND_SDSS_u=0, \
	BAND_SDSS_g=0,BAND_SDSS_r=0,BAND_SDSS_i=0, \
	BAND_SDSS_z=0,BOLOMETRIC=0, BAND_NUMBER=0, \
	VEGA=0, AB=0 ):
    return lmag.luminosity_to_magnitude( L, 
	UNITS_SOLAR_BOL=UNITS_SOLAR_BOL, UNITS_SOLAR_BAND=UNITS_SOLAR_BAND, 
	UNITS_CGS=UNITS_CGS, 
	NU_L_NU=NU_L_NU, L_NU=L_NU, 
	BAND_U=BAND_U,BAND_B=BAND_B,BAND_V=BAND_V,BAND_R=BAND_R,BAND_I=BAND_I,
	BAND_J=BAND_J,BAND_H=BAND_H,BAND_K=BAND_K,BAND_SDSS_u=BAND_SDSS_u, 
	BAND_SDSS_g=BAND_SDSS_g,BAND_SDSS_r=BAND_SDSS_r,BAND_SDSS_i=BAND_SDSS_i, 
	BAND_SDSS_z=BAND_SDSS_z,BOLOMETRIC=BOLOMETRIC, BAND_NUMBER=BAND_NUMBER, 
	VEGA=VEGA, AB=AB, MAGNITUDE_TO_LUMINOSITY=1)
	
	
## routines from attenuation module
def attenuate( nu_in_Hz, log_NH, metallicity_in_solar, \
        SMC=0, LMC=0, MW=0, BB=0, IR=0, SX=0, HX=0):
    return atten.attenuate( nu_in_Hz, log_NH, metallicity_in_solar, \
        SMC=SMC, LMC=LMC, MW=MW, BB=BB, IR=IR, SX=SX, HX=HX)
        
def cross_section( f0, METALLICITY_OVER_SOLAR=1.0 ):
    return atten_cx.cross_section( f0, METALLICITY_OVER_SOLAR=METALLICITY_OVER_SOLAR);
       
def opacity_per_solar_metallicity( f0 ):
    return atten_cx.opacity_per_solar_metallicity( f0 );
       
## routines from agn_spectrum module
def agn_spectrum( nu_in_Hz, log_l_bol, \
	BB=0, IR=0, SX=0, HX=0, \
	HRH=0, MARCONI=0, RICHARDS=0, \
	SDSS=0 ):
    return agnspec.agn_spectrum( nu_in_Hz, log_l_bol, \
	BB=BB, IR=IR, SX=SX, HX=HX, \
	HRH=HRH, MARCONI=MARCONI, RICHARDS=RICHARDS, \
	SDSS=SDSS )


## procedure which will return, for a given input vector A_in, 
##    the perpendicular unit vectors B_out and C_out 
##    which form perpendicular axes to A
def return_perp_vectors(a, LOUD=0):
    eps = 1.0e-10
    a = np.array(a,dtype='f');
    a /= np.sqrt(a[0]*a[0]+a[1]*a[1]+a[2]*a[2]);
    for i in range(len(a)):
        if (a[i]==0.): a[i]=eps;
        if (a[i]>=1.): a[i]=1.-eps;
        if (a[i]<=-1.): a[i]=-1.+eps;
    a /= np.sqrt(a[0]*a[0]+a[1]*a[1]+a[2]*a[2]);
    ax=a[0]; ay=a[1]; az=a[2];

    ## use a fixed rotation of the a-vector by 90 degrees:
    ## (this anchors the solution so it changes *continously* as a changes)
    t0=np.double(math.pi/2.e0);
    bx=0.*ax; by=np.cos(t0)*ay-np.sin(t0)*az; bz=np.sin(t0)*ay+np.cos(t0)*az;
    ## re-normalize to make sure these are unit vectors [other parts of our code do this already, but if not careful, can cause problems: better to do here]
    bmag = np.sqrt(bx**2 + by**2 + bz**2); bx/=bmag; by/=bmag; bz/=bmag;
    ## c-sign is degenerate even for 'well-chosen' a and b: gaurantee right-hand 
    ##  rule is obeyed by defining c as the cross product: a x b = c
    cx=(ay*bz-az*by); cy=-(ax*bz-az*bx); cz=(ax*by-ay*bx); 
    ## re-normalize to make sure these are unit vectors [other parts of our code do this already, but if not careful, can cause problems: better to do here]
    cmag = np.sqrt(cx**2 + cy**2 + cz**2); cx/=cmag; cy/=cmag; cz/=cmag;
    B_out=np.zeros(3); C_out=np.zeros(3);
    B_out[:]=[bx,by,bz]; C_out[:]=[cx,cy,cz];
    
    if (LOUD==1):
        print(a); 
        print(B_out); 
        print(C_out);
        print('a_tot=',ax*ax+ay*ay+az*az)
        print('b_tot=',bx*bx+by*by+bz*bz)
        print('c_tot=',cx*cx+cy*cy+cz*cz)
        print('adotb=',ax*bx+ay*by+az*bz)
        print('adotc=',ax*cx+ay*cy+az*cz)
        print('bdotc=',bx*cx+by*cy+bz*cz)
    
    return B_out, C_out
    
## vector nan checker (because I like dividing by zero, apparently)
def isnan(x):
    return np.isnan(x);

## more robust length checker (will catch scalars)
def checklen(x):
    return len(np.array(x,ndmin=1));

## round which guarantees an int result
def int_round(x):
    return np.int(np.round(x));

## generic tool for checking if values are ok
def ok_scan(input,xmax=1.0e10,pos=0):
    if (pos==1):
        return (np.isnan(input)==False) & (abs(input)<=xmax) & (input > 0.);
    else:
        return (np.isnan(input)==False) & (abs(input)<=xmax);

## return age of universe (for a flat universe) to a given redshift
def age_of_universe(z,h=0.71,Omega_M=0.27):
    ## exact solution for a flat universe
    a=1./(1.+z); x=Omega_M/(1.-Omega_M) / (a*a*a);
    t=(2./(3.*np.sqrt(1.-Omega_M))) * np.log( np.sqrt(x) / (-1. + np.sqrt(1.+x)) );
    t *= 13.777 * (0.71/h); ## in Gyr
    return t;

## return lookback time (for a flat universe) to a given redshift
def lookback_time(z,h=0.71,Omega_M=0.27):
    return age_of_universe(0.,h=h,Omega_M=Omega_M)-age_of_universe(z,h=h,Omega_M=Omega_M);

## simple moving-window signal smoothing (from scipy cookbook)
def smooth(x,window_len=11,window='hanning'):
        if x.ndim != 1:
                raise ValueError("smooth only accepts 1 dimension arrays.")
        if x.size < window_len:
                raise ValueError("Input vector needs to be bigger than window size.")
        if window_len<3:
                return x
        if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
                raise ValueError("Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")
        s=np.r_[2*x[0]-x[window_len-1::-1],x,2*x[-1]-x[-1:-window_len:-1]]
        if window == 'flat': #moving average
                w=np.ones(window_len,'d')
        else:  
                w=eval('np.'+window+'(window_len)')
        y=np.convolve(w/w.sum(),s,mode='same')
        return y[window_len:-window_len+1]

## linear interpolation, but with linear extrapolation for points beyond the data boundaries
##  (x_old is monotonically increasing, here)
def interp_w_extrap(x_new, x_old, y_old):
    y = np.interp(x_new,x_old,y_old)
    N = x_old.size - 1
    out = (x_new < x_old[0])
    if (y[out].size > 0):
        y[out] = y_old[0] + (y_old[1]-y_old[0])/(x_old[1]-x_old[0]) * (x_new[out]-x_old[0])
    out = (x_new > x_old[N])
    if (y[out].size > 0):
        y[out] = y_old[N] + (y_old[N]-y_old[N-1])/(x_old[N]-x_old[N-1]) * (x_new[out]-x_old[N])
    return y
    
## simple derivative function for y = y(x) [ x monotonically increasing ]
def my_simple_derivative(y_in, x_in):
    s = np.argsort(x_in)
    x = x_in[s]
    y = y_in[s]
    xm=x[0:x.size-1]
    xp=x[1:x.size]
    x_mid = 0.5*(xm+xp)
    dy_dx = np.diff(y)/np.diff(x)
    dy_dx_xin = interp_w_extrap( x, x_mid, dy_dx )
    z = 0.*y_in
    z[s] = dy_dx_xin
    return z
    
## return color based on an input number
def set_color(i):
    colors_vec=['black','blue','red','green','deepskyblue',\
        'darkviolet','orange','magenta','gold','sienna','pink',\
        'forestgreen','darkgray','lime','yellow','cyan','violet','gray',
        'purple','brown','cadetblue','antiquewhite','coral','cornsilk',
        'crimson','darkgoldenrod','indigo','midnightblue','olive','tan',
        'sandybrown','tomato','turquoise','springgreen','yellowgreen']
    if(i<0): i=0;
    if(i>=np.array(colors_vec).size): i=-1;
    return colors_vec[i];
    
## return marker based on an input number
def set_symbol(i):
    symbol_vec=['o','s','p','^','*','x','D','+',\
        'h','1','v','2','<','3','>','4','H']
    if(i<0): i=0;
    if(i>=np.array(symbol_vec).size): i=-1;
    return symbol_vec[i];

## return weighted inter-quartile or other range from e.g. 5-95% and mean or median
def weighted_iqr_mean(x_grid,x,u,wt,yr=[0.05,0.5,0.95],return_mean=False):
    umin=0.*x_grid; umax=0.*x_grid; umean=0.*x_grid;
    for i,x0 in zip(range(x_grid.size),x_grid):
        if(i==0): 
            dxm = (x_grid[i+1]-x_grid[i])/2.
        else:
            dxm = (x_grid[i]-x_grid[i-1])/2.

        if(i==x_grid.size-1): 
            dxp = (x_grid[i]-x_grid[i-1])/2.
        else:
            dxp = (x_grid[i+1]-x_grid[i])/2.

        ok=np.where((x <= x0+dxp) & (x >= x0-dxm))
        u_x=u.take(ok[0],axis=0)
        if(u_x.size >= 3):
            s=np.argsort(u_x); w=(wt.take(ok[0],axis=0))[s]; u_s=u_x[s]; w_c=np.cumsum(w); w_c/=np.max(w_c);
            umin[i]=np.interp(yr[0],w_c,u_s)
            umean[i]=np.interp(yr[1],w_c,u_s)
            umax[i]=np.interp(yr[2],w_c,u_s)
            if(return_mean):
                umean[i]=np.sum(u_s*w)/np.sum(w)
    return umin, umax, umean

## simple histogram to save us the diff operation
def myhistogram(u,**kwargs):
    y,xb = np.histogram(u[np.isfinite(u)],**kwargs)
    x = xb[0:-1] + 0.5*np.diff(xb)
    return y,x  

## fastkde algorithms here 
def fastkde(x, y, gridsize=(200, 200), extents=None, nocorrelation=False,
            weights=None, adjust=1.):
    """
    A fft-based Gaussian kernel density estimate (KDE)
    for computing the KDE on a regular grid
    Note that this is a different use case than scipy's original
    scipy.stats.kde.gaussian_kde
    IMPLEMENTATION
    --------------
    Performs a gaussian kernel density estimate over a regular grid using a
    convolution of the gaussian kernel with a 2D histogram of the data.
    It computes the sparse bi-dimensional histogram of two data samples where
    *x*, and *y* are 1-D sequences of the same length. If *weights* is None
    (default), this is a histogram of the number of occurences of the
    observations at (x[i], y[i]).
    histogram of the data is a faster implementation than numpy.histogram as it
    avoids intermediate copies and excessive memory usage!
    This function is typically *several orders of magnitude faster* than
    scipy.stats.kde.gaussian_kde.  For large (>1e7) numbers of points, it
    produces an essentially identical result.
    Boundary conditions on the data is corrected by using a symmetric /
    reflection condition. Hence the limits of the dataset does not affect the
    pdf estimate.
    Parameters
    ----------
    x, y:  ndarray[ndim=1]
        The x-coords, y-coords of the input data points respectively
    gridsize: tuple
        A (nx,ny) tuple of the size of the output grid (default: 200x200)
    extents: (xmin, xmax, ymin, ymax) tuple
        tuple of the extents of output grid (default: extent of input data)
    nocorrelation: bool
        If True, the correlation between the x and y coords will be ignored
        when preforming the KDE. (default: False)
    weights: ndarray[ndim=1]
        An array of the same shape as x & y that weights each sample (x_i,
        y_i) by each value in weights (w_i).  Defaults to an array of ones
        the same size as x & y. (default: None)
    adjust : float
        An adjustment factor for the bw. Bandwidth becomes bw * adjust.
    Returns
    -------
    g: ndarray[ndim=2]
        A gridded 2D kernel density estimate of the input points.
    e: (xmin, xmax, ymin, ymax) tuple
        Extents of g
    """
    # Variable check
    x, y = np.asarray(x), np.asarray(y)
    x, y = np.squeeze(x), np.squeeze(y)

    if x.size != y.size:
        raise ValueError('Input x & y arrays must be the same size!')

    n = x.size

    if weights is None:
        # Default: Weight all points equally
        weights = np.ones(n)
    else:
        weights = np.squeeze(np.asarray(weights))
        if weights.size != x.size:
            raise ValueError('Input weights must be an array of the same size as input x & y arrays!')

    # Optimize gridsize ------------------------------------------------------
    # Make grid and discretize the data and round it to the next power of 2
    # to optimize with the fft usage
    if gridsize is None:
        gridsize = np.asarray([np.max((len(x), 512.)), np.max((len(y), 512.))])
    gridsize = 2 ** np.ceil(np.log2(gridsize))  # round to next power of 2

    nx, ny = gridsize

    # Make the sparse 2d-histogram -------------------------------------------
    # Default extents are the extent of the data
    if extents is None:
        xmin, xmax = x.min(), x.max()
        ymin, ymax = y.min(), y.max()
    else:
        xmin, xmax, ymin, ymax = map(float, extents)
    dx = (xmax - xmin) / (nx - 1)
    dy = (ymax - ymin) / (ny - 1)

    # Basically, this is just doing what np.digitize does with one less copy
    # xyi contains the bins of each point as a 2d array [(xi,yi)]
    xyi = np.vstack((x, y)).T
    xyi -= [xmin, ymin]
    xyi /= [dx, dy]
    xyi = np.floor(xyi, xyi).T

    # Next, make a 2D histogram of x & y.
    # Exploit a sparse coo_matrix avoiding np.histogram2d due to excessive
    # memory usage with many points
    grid = coo_matrix((weights, xyi), shape=(int(nx), int(ny))).toarray()

    # Kernel Preliminary Calculations ---------------------------------------
    # Calculate the covariance matrix (in pixel coords)
    cov = np.cov(xyi)

    if nocorrelation:
        cov[1, 0] = 0
        cov[0, 1] = 0

    # Scaling factor for bandwidth
    scotts_factor = n ** (-1.0 / 6.) * adjust  # For 2D

    # Make the gaussian kernel ---------------------------------------------

    # First, determine the bandwidth using Scott's rule
    # (note that Silvermann's rule gives the # same value for 2d datasets)
    std_devs = np.sqrt(np.diag(cov))
    kern_nx, kern_ny = np.round(scotts_factor * 2 * np.pi * std_devs)

    # Determine the bandwidth to use for the gaussian kernel
    inv_cov = np.linalg.inv(cov * scotts_factor ** 2)

    # x & y (pixel) coords of the kernel grid, with <x,y> = <0,0> in center
    xx = np.arange(kern_nx, dtype=np.float) - kern_nx / 2.0
    yy = np.arange(kern_ny, dtype=np.float) - kern_ny / 2.0
    xx, yy = np.meshgrid(xx, yy)

    # Then evaluate the gaussian function on the kernel grid
    kernel = np.vstack((xx.flatten(), yy.flatten()))
    kernel = np.dot(inv_cov, kernel) * kernel
    kernel = np.sum(kernel, axis=0) / 2.0
    kernel = np.exp(-kernel)
    kernel = kernel.reshape((int(kern_ny), int(kern_nx)))

    # ---- Produce the kernel density estimate --------------------------------

    # Convolve the histogram with the gaussian kernel
    # use boundary=symm to correct for data boundaries in the kde
    grid = convolve2d(grid, kernel, mode='same', boundary='symm')

    # Normalization factor to divide result by so that units are in the same
    # units as scipy.stats.kde.gaussian_kde's output.
    norm_factor = 2 * np.pi * cov * scotts_factor ** 2
    norm_factor = np.linalg.det(norm_factor)
    norm_factor = n * dx * dy * np.sqrt(norm_factor)

    # Normalize the result
    grid /= norm_factor

    return grid, (xmin, xmax, ymin, ymax)


def fastkde1D(xin, gridsize=200, extents=None, weights=None, adjust=1.):
    """
    A fft-based Gaussian kernel density estimate (KDE)
    for computing the KDE on a regular grid
    Note that this is a different use case than scipy's original
    scipy.stats.kde.gaussian_kde
    IMPLEMENTATION
    --------------
    Performs a gaussian kernel density estimate over a regular grid using a
    convolution of the gaussian kernel with a 2D histogram of the data.
    It computes the sparse bi-dimensional histogram of two data samples where
    *x*, and *y* are 1-D sequences of the same length. If *weights* is None
    (default), this is a histogram of the number of occurences of the
    observations at (x[i], y[i]).
    histogram of the data is a faster implementation than numpy.histogram as it
    avoids intermediate copies and excessive memory usage!
    This function is typically *several orders of magnitude faster* than
    scipy.stats.kde.gaussian_kde.  For large (>1e7) numbers of points, it
    produces an essentially identical result.
    **Example usage and timing**
        from scipy.stats import gaussian_kde
        def npkde(x, xe):
            kde = gaussian_kde(x)
            r = kde.evaluate(xe)
            return r
        x = np.random.normal(0, 1, 1e6)
        %timeit fastkde1D(x)
        10 loops, best of 3: 31.9 ms per loop
        %timeit npkde(x, xe)
        1 loops, best of 3: 11.8 s per loop
        ~ 1e4 speed up!!! However gaussian_kde is not optimized for this
        application
    Boundary conditions on the data is corrected by using a symmetric /
    reflection condition. Hence the limits of the dataset does not affect the
    pdf estimate.
    Parameters
    ----------
        xin:  ndarray[ndim=1]
            The x-coords, y-coords of the input data points respectively
        gridsize: int
            A nx integer of the size of the output grid (default: 200x200)
        extents: (xmin, xmax) tuple
            tuple of the extents of output grid (default: extent of input data)
        weights: ndarray[ndim=1]
            An array of the same shape as x that weights each sample x_i
            by w_i.  Defaults to an array of ones the same size as x
            (default: None)
        adjust : float
            An adjustment factor for the bw. Bandwidth becomes bw * adjust.
    Returns
    -------
        g: ndarray[ndim=2]
            A gridded 2D kernel density estimate of the input points.
        e: (xmin, xmax, ymin, ymax) tuple
            Extents of g
    """
    # Variable check
    x = np.squeeze(np.asarray(xin))

    # Default extents are the extent of the data
    if extents is None:
        xmin, xmax = x.min(), x.max()
    else:
        xmin, xmax = map(float, extents)
        x = x[(x <= xmax) & (x >= xmin)]

    n = x.size

    if weights is None:
        # Default: Weight all points equally
        weights = np.ones(n)
    else:
        weights = np.squeeze(np.asarray(weights))
        if weights.size != x.size:
            raise ValueError('Input weights must be an array of the same size as input x & y arrays!')

    # Optimize gridsize ------------------------------------------------------
    # Make grid and discretize the data and round it to the next power of 2
    # to optimize with the fft usage
    if gridsize is None:
        gridsize = np.max((len(x), 512.))
    gridsize = 2 ** np.ceil(np.log2(gridsize))  # round to next power of 2

    nx = int(gridsize)

    # Make the sparse 2d-histogram -------------------------------------------
    dx = (xmax - xmin) / (nx - 1)

    # Basically, this is just doing what np.digitize does with one less copy
    # xyi contains the bins of each point as a 2d array [(xi,yi)]
    xyi = x - xmin
    xyi /= dx
    xyi = np.floor(xyi, xyi)
    xyi = np.vstack((xyi, np.zeros(n, dtype=int)))

    # Next, make a 2D histogram of x & y.
    # Exploit a sparse coo_matrix avoiding np.histogram2d due to excessive
    # memory usage with many points
    grid = coo_matrix((weights, xyi), shape=(int(nx), 1)).toarray()

    # Kernel Preliminary Calculations ---------------------------------------
    std_x = np.std(xyi[0])

    # Scaling factor for bandwidth
    scotts_factor = n ** (-1. / 5.) * adjust  # For n ** (-1. / (d + 4))

    # Silvermann n * (d + 2) / 4.)**(-1. / (d + 4)).

    # Make the gaussian kernel ---------------------------------------------

    # First, determine the bandwidth using Scott's rule
    # (note that Silvermann's rule gives the # same value for 2d datasets)
    kern_nx = int(np.round(scotts_factor * 2 * np.pi * std_x))

    # Then evaluate the gaussian function on the kernel grid
    kernel = np.reshape(gaussian(kern_nx, scotts_factor * std_x), (kern_nx, 1))

    # ---- Produce the kernel density estimate --------------------------------

    # Convolve the histogram with the gaussian kernel
    # use symmetric padding to correct for data boundaries in the kde
    npad = np.min((nx, 2 * kern_nx))
    grid = np.vstack([grid[npad: 0: -1], grid, grid[nx: nx - npad: -1]])
    grid = convolve(grid, kernel, mode='same')[npad: npad + nx]

    # Normalization factor to divide result by so that units are in the same
    # units as scipy.stats.kde.gaussian_kde's output.
    norm_factor = 2 * np.pi * std_x * std_x * scotts_factor ** 2
    norm_factor = n * dx * np.sqrt(norm_factor)

    # Normalize the result
    grid /= norm_factor

    return np.squeeze(grid), (xmin, xmax)


